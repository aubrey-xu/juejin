/**
 * @description: 节流
 * @param {fun} fn - 要节流的函数
 * @param {Number} delay - 节流时间
 * @param {all} args - 传递的参数
 */
export default function throttle(fn, delay = 300) {
  let timer = null
  return function (...args) {
    if (timer) return
    timer = setTimeout(() => {
      fn.apply(this, args)
      timer = 0
    }, delay)
  }
}
