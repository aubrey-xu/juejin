/**
 * @description: 防抖
 * @param {fun} fn - 要防抖的函数
 * @param {Number} delay - 防抖时间
 * @param {all} args - 传递的参数
 */
export default function debounce(fn, delay = 300) {
  let timer = 0
  return function (...args) {
    if (timer) clearTimeout(timer)
      
    timer = setTimeout(() => {
      fn.apply(this, args)
      timer = 0
    }, delay)
  }
}
