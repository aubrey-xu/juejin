// 滚动事件的监听与销毁

import throttle from '@/utils/throttle'
export default {
  data() {
    return {
      // 滚动前，滚动条距文档顶部的距离
      oldScrollTop: 0
    }
  },
  created() {
    // 开启节流,监听滚动
    // 这么写是为了能够暴露出监听事件,方便销毁
    this.listenScroll = throttle(this.scrolling, 200)
  },
  mounted() {
    window.addEventListener('scroll', this.listenScroll)
  },
  beforeDestroy() {
    // 解除监听
    window.removeEventListener('scroll', this.listenScroll)
  }
}
