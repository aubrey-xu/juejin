import axios from 'axios'

const BASE_URL = process.env.NODE_ENV === 'development' ? 'http://localhost:8000/api/v1' : 'http://43.138.78.53:8000/api/v1'

export function myAxios(config) {
  // 实例化 axios
  const instance = axios.create({
    baseURL: BASE_URL,
    timeout: 5000
  })
  // 默认配置
  instance.defaults.headers.common['Content-Type'] = 'application/x-www-form-urlencoded'
  return instance(config)
}
