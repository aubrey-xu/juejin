import Vue from 'vue'
import {
  Divider,
  Select,
  Option,
  Skeleton,
  SkeletonItem,
  Message,
  Badge,
  Input,
  Dropdown,
  DropdownItem,
  DropdownMenu
} from 'element-ui'

Vue.use(Divider)
Vue.use(Select)
Vue.use(Option)
Vue.use(Skeleton)
Vue.use(SkeletonItem)
Vue.use(Badge)
Vue.use(Input)
Vue.use(Dropdown)
Vue.use(DropdownItem)
Vue.use(DropdownMenu)

Vue.prototype.$message = Message
