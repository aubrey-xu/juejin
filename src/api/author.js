import { myAxios } from '../plugins/myAxios'

/**
 * @description: 获取作者榜单
 * @param {num} category_id - 分类 id，默认获取推荐作者榜单，传入分类 id，可获取对应分类下作者榜单
 * @param {string} cursor - 分页标识
 * @param {num} limit  - 限制条数，默认 20
 */
export const reqArticleTop = (params) => {
  return myAxios({
    url: '/author/recommend',
    params
  })
}
