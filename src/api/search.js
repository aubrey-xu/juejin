import { myAxios } from '../plugins/myAxios'

/**
 * @description: 搜索数据
 * @param {num} id_type - 分类 id，0：综合 1：用户 2：文章 9：标签
 * @param {string} key_word - 关键词
 * @param {num} limit  - 限制条数，默认 20
 * @param {num} search_type  - 搜索类型 - 0：全部 1：一天内 2：一周内 3：三个月内
 */
export const reqSearch = (params) => {
  return myAxios({
    url: '/articles/search',
    params
  })
}      