import { myAxios } from '../plugins/myAxios'

// /**
//  * @description:  获取首页广告信息
//  * @param {num} position - 优先度 - 目前100和0
//  * @param {num} platform - 未知 - 先用 2608 顶替
//  * @param {num} layout - 广告布局 - 1：正方形大广告,2：为横屏小广告
//  */
// export const reqAD = () => {
//   return myAxios({
//     method: 'POST',
//     url: '/content_api/v1/advert/query_adverts',
//     params: {
//       position: 100,
//       platform: 2608,
//       layout: 2
//     }
//   })
// }

// 广告直接用我的图床了,不想写接口

/**
 * @description: 获取首页文章列表
 * @param {num} id_type - 文章类别 - 官方广告：14,普通文章：2
 * @param {num} client_type - 未知 -默认为2608
 * @param {num} sort_type - 排序类别
 * 推荐：200
 * 最新：300
 * 热榜3天内：3
 * 热榜7天内：7
 * 热榜30天内：30
 * 热榜全部：0
 * @param {string} cursor - 偏移量 - 未知
 * @param {num} limit - 请求数量
 */
export const reqArticle = (params) => {
  return myAxios({
    method: 'POST',
    url: 'articles/index_list',
    params
  })
}

/**
 * @description: 获取分类列表
 * @param {num} show_type - 查询参数 - 首页 1：作者排行榜 2：小册
 */
export const reqCate = (show_type = 1) => {
  return myAxios({
    url: '/categories/list',
    params: { show_type }
  })
}

/**
 * @description: 获取某个分类下的标签
 * @param {num} cate_id - 分类 id
 */
export const reqCateChild = (cate_id) => {
  return myAxios({
    method: 'POST',
    url: '/categories/tags',
    params: { cate_id }
  })
}
