import { myAxios } from '../plugins/myAxios'

/**
 * @description: 获取文章详情
 * @param {num} article_id - 文章ID
 */
export const reqArticleDetail = (article_id) => {
  return myAxios({
    url: '/articles/detail',
    params: { article_id }
  })
}

/**
 * @description: 获取相似文章
 * @param {num} item_id - 文章ID
 * @param {num} user_id - 用户ID
 * @param {num} tag_ids - 标签 id - 可传多个，数组字符串形式传入，例如："tag_ids":["6809640399544516616", "6809640501776482317"]
 */
export const reqArticleSimilar = (item_id) => {
  return myAxios({
    method: 'POST',
    url: '/articles/related_entry',
    params: { item_id }
  })
}

/**
 * @description: 获取相关推荐文章
 * @param {num} item_id  - 文章ID
 * @param {num} sort_type   - 排序类型 - 0：全部 3：三天内 7：7天内 30：30天内 200：热门 300：最新
 * @param {string} cursor   - 分页标识
 * @param {num} tag_ids   -  标签 id -可传多个，数组字符串形式传入，例如："tag_ids":["6809640399544516616", "6809640501776482317"]
 */
export const reqArticleRecommend = (item_id) => {
  return myAxios({
    method: 'POST',
    url: '/articles/recommend_entry_by_tag_ids',
    params: { item_id }
  })
}
