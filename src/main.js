import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './plugins/element.js'
import '@/assets/css/globe.css'
import '@/assets/css/media.css'
import '@/assets/css/markdown.min.css'
import { timeago } from '@/utils/dateAgo'
import { dateFormat } from '@/utils/dateFormat'

import highlight from 'highlight.js'
import '@/assets/css/high.min.css'

import VueLazyload from 'vue-lazyload'

// 转换时间戳
Vue.filter('timeago', timeago)
Vue.filter('dateFormat', dateFormat)
Vue.prototype.$bus = new Vue()

// 图片懒加载的配置
Vue.use(VueLazyload, {
  loading: require('./assets/img/lazy/lazy.png')
})

// 代码高亮
Vue.use(highlight)
Vue.directive('highlight', (el) => {
  let blocks = el.querySelectorAll('pre code')
  blocks.forEach((block) => {
    highlight.highlightBlock(block)
  })
})

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: (h) => h(App)
}).$mount('#app')
