import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // header 是否隐藏
    isHideHead: false,
    // 首页是否滚动到底部
    isArriveBottom: false,
    // 是否显示首页固定的广告
    isShowAD: false,
    // 文章页的目录是否开启定位
    shouldFixed: false,
    // 主页选中的导航栏各个级别的 index
    navHomeChecked: JSON.parse(
      window.sessionStorage.getItem('nav-home-checked')
    ) || { father: -2, child: 0 },
    // 分类子栏的数据
    cateChild: [],
    // 分类父栏的数据
    cateList: []
  },
  getters: {},
  mutations: {
    // 更新 header 是否隐藏
    updateIsHideHead(state, newState) {
      state.isHideHead = newState
      // console.log('header新状态:' + newState)
    },
    // 更新首页是否滚动到底部
    updateIsArriveBottom(state, newState) {
      state.isArriveBottom = newState
      // console.log('按钮新状态:' + newState)
    },
    // 更新是否显示首页固定的广告
    updateisShowAD(state, newState) {
      state.isShowAD = newState
      // console.log('AD新状态:' + newState)
    },
    // 文章页的目录是否开启定位
    updateShouldFixed(state, newState) {
      state.shouldFixed = newState
      // console.log('shouldFixed:' + newState)
    },
    // 更新主页选中的导航栏各个级别的 index
    updateNavHomeChecked(state, newState) {
      state.navHomeChecked = newState
    },
    // 更新对应分类子栏的数据
    updateCateChild(state, newState) {
      state.cateChild = newState
    },
    // 更新父栏的数据
    updateCateList(state, newState) {
      state.cateList = newState
      console.log('更新')
    }
  },
  actions: {},
  modules: {}
})
