import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import(/* WebpackChunkName: "home" */ '@/views/home/Home')
  },
  {
    path: '/article',
    name: 'article',
    component: () =>
      import(/* WebpackChunkName: "article" */ '@/views/article/Article')
  },
  {
    path: '/search',
    name: 'search',
    component: () =>
      import(/* WebpackChunkName: "search" */ '@/views/search/Serach')
  }
]

// 解决携带参数在同页面跳转时，路由报错的问题
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function (location) {
  return originalPush.call(this, location).catch((err) => err)
}

const router = new VueRouter({
  routes
})

export default router
