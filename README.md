# 淘金队-仿掘金官网


## 已完成页面功能点

1. ### 全局功能

   1. ### 开启节流，滚动监听

   2. ### 上拉加载，请求唯一

   3. ### 图片懒加载，路由懒加载

   4. ### 开启骨架屏

   5. ### 回到顶部

   6. ...

2. ### 首页

   1. ### 分类栏 hover 防抖，防闪烁

   2. ### 多分类栏切换与刷新保存

   3. ### 网页动效，尽量契合官网

   4. ### 文章跳转

   5. ...

3. ### 文章详情页

   1. ### 文章锚点

   2. ### 本页文章跳转

   3. ...

4. ### 搜索页

   1. ### 文章搜索

   2. ### 用户搜索

   3. ### 标签搜索

   4. ...

5. ### 移动端适配

## 效果预览

### 1. 主页分类

![homeCate.gif](https://image.xumaobin.xyz/2022/08/25/3dae3d8a0d452.gif)

### 2. 主页滚动

![homeScrollgif.gif](https://image.xumaobin.xyz/2022/08/25/fb42e61a196a4.gif)

### 3. 搜索

![search.gif](https://image.xumaobin.xyz/2022/08/25/2a857845d1812.gif)

### 4. 文章锚点

![article.gif](https://image.xumaobin.xyz/2022/08/21/965ae55af5acf.gif)

### 5. 文章跳转

![goto.gif](https://image.xumaobin.xyz/2022/08/21/f587e2e6427aa.gif)

### 



### 6. 移动端表现

1. ![mobie1gif.gif](https://image.xumaobin.xyz/2022/08/25/7cb8d604cced2.gif)
2. <img src="https://image.xumaobin.xyz/2022/08/21/f0a259d91eac9.gif" alt="mobie2.gif" style="zoom:50%;" />

## 文件目录

```
├─.browserslistrc 
├─.eslintrc.js 
├─.gitignore 
├─babel.config.js 
├─jsconfig.json 
├─package-lock.json 
├─package.json 
├─public 
│ ├─favicon.ico 
│ └─index.html 
├─README.md 
├─src 
│ ├─api ---------------------------- // 接口
│ │ ├─article.js ------------------- // 文章相关
│ │ ├─author.js -------------------- // 作者相关
│ │ ├─home.js ---------------------- // 主页所需
│ │ └─search.js -------------------- // 搜索相关
│ ├─App.vue 
│ ├─assets 
│ │ ├─css 
│ │ │ ├─globe.css ------------------ // 全局样式
│ │ │ ├─high.min.css --------------- // 代码高亮样式-开发时使用
│ │ │ ├─markdown.min.css ----------- // markdown文章样式
│ │ │ └─media.css ------------------ // 媒体查询
│ │ └─img 
│ ├─components --------------------- // 可复用组件
│ │ ├─articlesList 
│ │ │ ├─ArticlesList.vue ----------- // 文章列表
│ │ │ └─componets 
│ │ │   ├─SearchArticle.vue -------- // 搜索-文章
│ │ │   ├─SearchTag.vue ------------ // 搜索-标签 
│ │ │   └─SearchUser.vue ----------- // 搜索-用户
│ │ ├─header 
│ │ │ └─Header.vue ----------------- // 头部
│ │ ├─navList 
│ │ │ └─NavList.vue ---------------- // 导航栏
│ │ ├─sideButton 
│ │ │ └─sidewButton.vue ------------ // 右下角按钮
│ │ └─skeleton 
│ │   └─Skeleton.vue --------------- // 骨架屏
│ ├─main-prod.js 
│ ├─main.js 
│ ├─mixins ------------------------- // 混合滚动事件
│ │ └─scroll.js 
│ ├─plugins 
│ │ ├─element.js ------------------- // element-ui 按需导入
│ │ └─myAxios.js ------------------- // axios 配置
│ ├─router 
│ │ └─index.js --------------------- // 路由
│ ├─store 
│ │ └─index.js --------------------- // vuex 状态管理
│ ├─utils -------------------------- // 工具函数
│ │ ├─dateAgo.js ------------------- // 返回多久之前
│ │ ├─dateFormat.js ---------------- // 返回日期格式
│ │ ├─debounce.js ------------------ // 防抖
│ │ └─throttle.js ------------------ // 节流
│ └─views 
│   ├─article 
│   │ ├─Article.vue ---------------- // 文章页面
│   │ └─componets 
│   │   ├─ArticleAsideLeft.vue ----- // 文章左侧
│   │   ├─ArticleAsideRight.vue ---- // 文章右侧
│   │   └─ArticleMain.vue ---------- // 文章页面
│   ├─home 
│   │ ├─componets 
│   │ │ ├─HomeAside.vue 
│   │ │ ├─HomeMain.vue 
│   │ │ └─HomeNavChild.vue 
│   │ └─Home.vue ------------------- // 主页面
│   └─search 
│     ├─componets 
│     │ └─SearchMain.vue 
│     └─Serach.vue ----------------- // 搜索页面
└─vue.config.js 
```

### 吐槽：一核干活，多核围观:crying_cat_face:
