const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  lintOnSave: false,
  chainWebpack: (config) => {
    // 发布模式
    config.when(process.env.NODE_ENV === 'production', (config) => {
      config.entry('app').clear().add('./src/main-prod.js')
      config.set('externals', {
        vue: 'Vue',
        'vue-router': 'VueRouter',
        axios: 'axios',
        'vue-lazyload': 'VueLazyload',
        marked: 'marked',
        'highlight.js': 'hljs'
      })
      config.plugin('html').tap((args) => {
        args[0].isProd = true
        return args
      })
      // 清除 log
      config.optimization.minimizer('terser').tap((args) => {
        args[0].parallel = 4
        args[0].terserOptions.compress.warnings = true
        args[0].terserOptions.compress.drop_debugger = true
        args[0].terserOptions.compress.drop_console = true
        return args
      })
    })
    // 开发模式
    config.when(process.env.NODE_ENV === 'development', (config) => {
      config.entry('app').clear().add('./src/main.js')
      config.plugin('html').tap((args) => {
        args[0].isProd = false
        return args
      })
    })
  }
})
